# cypress bookstore
clone terlebih dahulu
 git clone https://gitlab.com/hafifbustani/cypress-bookstore.git

 setelah di lokal masuk ke direktori repository
 jika sudah ada node js, lakukan perintah
 npx cypress open
 pilih e2e test, pilih spec nya

 ada 3 proses
 1. beforeAll
 didalam before all isinya adalah menghapus data yang sebelumnya (jika ada)
 terdiri dari 2 endpoint
 get account book
 delete account book

 2. assign book to account/userid
 terdiri dari 1 endpoint, post book

 3. sama seperti point 2, hanya saja akan ada handler error karena sudah already exist