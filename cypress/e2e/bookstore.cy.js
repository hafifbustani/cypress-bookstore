describe('bookstore CRUD', () => {
  const userId = '6dcd2bf2-b153-483c-89cd-4f93abc34003', isbn = "9781449325862", basicAuth = 'Basic aGFmaWZ0ZXN0OkhhZmlmMTIzNDU2Xg'
  before(() => {
    cy.getAccountBook(basicAuth).then('check books',(response)=>{
      if (response.body.books.length > 0) {
        cy.deleteBook(basicAuth,isbn,userId)
      }
    })
  });

  it('POST Books', () => {
    cy.createboook(basicAuth,isbn,userId)
  })

  it('POST duplicate Books', () => {
    const options = {
      method: 'POST',
      url: 'https://bookstore.toolsqa.com/bookstore/v1/books',
      headers: {
        authorization: basicAuth,
      },
      body: {
        "userId": userId,
        "collectionOfIsbns": [
          {
            "isbn": isbn
          }
        ]
      }, failOnStatusCode: false
    }
    cy.request(options).should(response => {
      expect(response.status).to.equal(400)
      expect(response.body.message).to.equal("ISBN already present in the User's Collection!")
    })
  })
})