// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })
Cypress.Commands.add('deleteBook', (basicAuth,isbn,userid) => {
  const optionsDel = {
    method: 'DELETE',
    url: 'https://bookstore.toolsqa.com/bookstore/v1/book',
    headers: {
      authorization: basicAuth,
    }, body: {
      isbn: isbn,
      userId: userid
    }
  }
  cy.request(optionsDel).should(response => {
    expect(response.status).to.equal(204);
  })
});

Cypress.Commands.add('createboook', (basicAuth,isbn,userid) => {
  const options = {
    method: 'POST',
    url: 'https://bookstore.toolsqa.com/bookstore/v1/books',
    headers: {
      authorization: basicAuth,
    },
    body: {
      "userId": userid,
      "collectionOfIsbns": [
        {
          "isbn": isbn
        }
      ]
    }
  }
  cy.request(options).should(response => {
    expect(response.status).to.equal(201)
  })
});

Cypress.Commands.add('getAccountBook', (basicAuth) => {
  const options = {
    method: 'GET',
    url: 'https://bookstore.toolsqa.com/account/v1/user/6dcd2bf2-b153-483c-89cd-4f93abc34003',
    headers: {
      authorization: basicAuth,
    }
  }
  cy.request(options).should(response => {
    expect(response.status).to.equal(200)
    return response
  })
});